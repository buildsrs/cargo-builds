use clap::Parser;
use url::Url;

#[derive(Parser, Debug)]
pub struct Options {
    /// API to connect to for builds.
    #[clap(long, short, env = "CARGO_BUILDS_API", default_value = "https://api.builds/rs")]
    pub builds_api: Url,

    /// Name of crate to fetch
    pub crate_name: String,
}

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let options = Options::parse();
    println!("TODO");
}
